package com.yazid.RestApiKampus.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.yazid.RestApiKampus.models.DataNilai;
import com.yazid.RestApiKampus.models.Dosen;
import com.yazid.RestApiKampus.models.Mahasiswa;
import com.yazid.RestApiKampus.models.MataKuliah;
import com.yazid.RestApiKampus.models.dto.DataNilaiDTO;
import com.yazid.RestApiKampus.models.dto.DataNilaiMahasiswa;
import com.yazid.RestApiKampus.repositories.DataNilaiRepository;
import com.yazid.RestApiKampus.repositories.DosenRepository;
import com.yazid.RestApiKampus.repositories.MahasiswaRepository;
import com.yazid.RestApiKampus.repositories.MataKuliahRepository;

@RestController
@RequestMapping("/dataNilai")
public class DataNilaiController {

	@Autowired
	DataNilaiRepository dataNilaiRepository;

	@Autowired
	DosenRepository dosenRepository;

	@Autowired
	MataKuliahRepository mataKuliahRepository;

	@Autowired
	MahasiswaRepository mahasiswaRepository;

	@PreAuthorize("hasRole('ROLE_Dosen')")
	@RequestMapping(value="/createDataNilai", method = RequestMethod.POST)
	public HashMap<String, Object> createDataNilai(@Valid @RequestBody DataNilaiDTO body){
		HashMap<String, Object> result = new HashMap<String, Object>();

		DataNilai entity = new DataNilai();
		Dosen dosen = dosenRepository.getByDosenNama(body.getNamaDosen());
		MataKuliah mataKuliah = mataKuliahRepository.getByMatkulNama(body.getNamaMataKuliah(), body.getNim());
		Mahasiswa mahasiswa = mahasiswaRepository.getById(body.getNim());

		entity.setNim(mahasiswa);
		entity.setDosen(dosen);
		entity.setMataKuliah(mataKuliah);
		entity.setNilai(body.getNilai());
		entity.setKeterangan(body.getKeterangan());

		dataNilaiRepository.save(entity);

		result.put("Status", 200);
		result.put("Message", "Success ! Save Data Nilai");
		result.put("Data", entity);

		return result;
	}

	@PreAuthorize("hasRole('ROLE_Dosen')")
	@RequestMapping(value="/updateDataNilai/{id}", method = RequestMethod.PUT)
	public HashMap<String, Object> updateDataNilai(@PathVariable(value = "id") Long id, @Valid @RequestBody DataNilaiDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();

		DataNilai entity = dataNilaiRepository.getById(id);
		Dosen dosen = dosenRepository.getByDosenNama(body.getNamaDosen());
		MataKuliah mataKuliah = mataKuliahRepository.getByMatkulNama(body.getNamaMataKuliah(), body.getNim());
		Mahasiswa mahasiswa = mahasiswaRepository.getById(body.getNim());

		entity.setNim(mahasiswa);
		entity.setDosen(dosen);
		entity.setMataKuliah(mataKuliah);
		entity.setNilai(body.getNilai());
		entity.setKeterangan(body.getKeterangan());

		dataNilaiRepository.save(entity);

		result.put("Status", 200);
		result.put("Message", "Success ! Update Data Nilai");
		result.put("Data", entity);

		return result;
	}

	@PreAuthorize("hasRole('ROLE_Dosen')")
	@RequestMapping(value="/deleteDataNilai/{id}", method = RequestMethod.DELETE)
	public HashMap<String, Object> deleteDataNilai(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> result = new HashMap<String, Object>();

		DataNilai entity = dataNilaiRepository.getById(id);

		dataNilaiRepository.delete(entity);

		result.put("Status", 200);
		result.put("Message", "Success ! Delete Data Nilai");

		return result;
	}

	@RequestMapping(value = "/showDataNilaiMahasiswa", method = RequestMethod.GET)
	public HashMap<String, Object> showDataNilaiMahasiswa() {
		HashMap<String, Object> result = new HashMap<String, Object>();

		List<DataNilai> listDataNilai = dataNilaiRepository.getDataNilaiMahasiswa();
		List<DataNilaiMahasiswa> listDataNilaiMahasiswa = new ArrayList<DataNilaiMahasiswa>();

		for (DataNilai dataNilai : listDataNilai) {
			DataNilaiMahasiswa dataNilaiMahasiswa = new DataNilaiMahasiswa();
			dataNilaiMahasiswa.setNim(dataNilai.getNim().getNim());
			dataNilaiMahasiswa.setNama(dataNilai.getNim().getNama());
			dataNilaiMahasiswa.setJurusan(dataNilai.getNim().getJurusan());
			dataNilaiMahasiswa.setUmur(new Date().getYear() - dataNilai.getNim().getTanggalLahir().getYear());
			dataNilaiMahasiswa.setDosen(dataNilai.getDosen().getNama());
			dataNilaiMahasiswa.setNamaMataKuliah(dataNilai.getMataKuliah().getNamaMataKuliah());
			dataNilaiMahasiswa.setNilai(dataNilai.getNilai());

			listDataNilaiMahasiswa.add(dataNilaiMahasiswa);
		}

		result.put("Status", 200);
		result.put("Message", "Success ! Show Data Nilai Mahasiswa");
		result.put("Data", listDataNilaiMahasiswa);

		return result;
	}

	@RequestMapping(value = "/showRataRataSetiapMahasiswa", method = RequestMethod.GET)
	public HashMap<String, Object> showRataRataSetiapMahasiswa() {
		HashMap<String, Object> result = new HashMap<String, Object>();

		List<Map<String,Object>> listDataNilai = dataNilaiRepository.getRataRataByMahasiswa();

		result.put("Status", 200);
		result.put("Message", "Success ! Show Data Nilai Rata Rata Setiap Mahasiswa");
		result.put("Data", listDataNilai);

		return result;
	}

	@RequestMapping(value = "/showRataRataSetiapJurusan", method = RequestMethod.GET)
	public HashMap<String, Object> showRataRataSetiapJurusan() {
		HashMap<String, Object> result = new HashMap<String, Object>();

		List<Map<String,Object>> listDataNilai = dataNilaiRepository.getRataRataByJurusan();

		result.put("Status", 200);
		result.put("Message", "Success ! Show Data Nilai Rata Rata Setiap Jurusan");
		result.put("Data", listDataNilai);

		return result;
	}
}
