package com.yazid.RestApiKampus.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.yazid.RestApiKampus.models.Mahasiswa;
import com.yazid.RestApiKampus.repositories.MahasiswaRepository;

@RestController
@RequestMapping("/mahasiswa")
public class MahasiswaController {
	
	@Autowired
	MahasiswaRepository mahasiswaRepository;

	@PreAuthorize("hasRole('ROLE_Dosen')")
    @RequestMapping(value="/uploadDataMahasiswa", method = RequestMethod.POST)
	public HashMap<String, Object> uploadDataMahasiswa(@RequestParam("file") MultipartFile file) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		List<Mahasiswa> listMahasiswa = new ArrayList<Mahasiswa>();
		
		try {
			String path = System.getProperty("user.dir");
			File tempFile = new File(path+"\\Excel File\\tempFile.xlsx");

			file.transferTo(tempFile);
			
			FileInputStream fis = new FileInputStream(tempFile);
			
			XSSFWorkbook workbook = new XSSFWorkbook(fis);
			XSSFRow row;
			XSSFCell cell;
			XSSFSheet sheet = workbook.getSheetAt(0);
			
			Iterator<Row> rowIterator = sheet.iterator();
			int i = 1;
			while (rowIterator.hasNext()) {
				Mahasiswa mahasiswa = new Mahasiswa();
				row = sheet.getRow(i);
				if (row == null) {
					break;
				}
				cell = row.getCell(0);
				mahasiswa.setNim(cell.getStringCellValue().trim());
				cell = row.getCell(1);
				mahasiswa.setAlamat(cell.getStringCellValue().trim());;
				cell = row.getCell(2);
				mahasiswa.setJurusan(cell.getStringCellValue().trim());
				cell = row.getCell(3);
				mahasiswa.setNama(cell.getStringCellValue().trim());
				cell = row.getCell(4);
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				Date date = sdf.parse(cell.getStringCellValue().trim());
				mahasiswa.setTanggalLahir(date);
				
				listMahasiswa.add(mahasiswa);
				
				i++;
			}
			fis.close();
			workbook.close();
		} catch (Exception e) {
            e.printStackTrace();
        }
		
		mahasiswaRepository.saveAll(listMahasiswa);
		
		result.put("Data", listMahasiswa);
		
		return result;
	}
}
