package com.yazid.RestApiKampus.services;

import org.springframework.beans.factory.annotation.Autowired;

import com.yazid.RestApiKampus.models.Role;
import com.yazid.RestApiKampus.repositories.RoleRepository;

public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleRepository roleRepository;

	@Override
	public Role findByName(String name) {
		Role role = roleRepository.findByName(name);
		return role;
	}
}
