package com.yazid.RestApiKampus.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.yazid.RestApiKampus.models.Role;
import com.yazid.RestApiKampus.models.UserModel;
import com.yazid.RestApiKampus.models.dto.UserModelDTO;
import com.yazid.RestApiKampus.repositories.UserRepository;

public class UserServiceImpl implements UserDetailsService, UserService {

	@Autowired
    private RoleService roleService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bcryptEncoder;

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserModel user = userRepository.findByEmail(username);
        if(user == null){
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), getAuthority(user));
    }

    private Set<SimpleGrantedAuthority> getAuthority(UserModel user) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        Role role = user.getRoles();
        authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
        return authorities;
    }

    public List<UserModel> findAll() {
        List<UserModel> list = new ArrayList<>();
        userRepository.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public UserModel findOne(String username) {
        return userRepository.findByEmail(username);
    }

       @Override
    public UserModel save(UserModelDTO user) {

    	UserModel nUser = new UserModel();
    	nUser.setEmail(user.getEmail());
        nUser.setPassword(bcryptEncoder.encode(user.getPassword()));

        Role role = roleService.findByName(user.getRole());

        nUser.setRoles(role);
        return userRepository.save(nUser);
    }
}
