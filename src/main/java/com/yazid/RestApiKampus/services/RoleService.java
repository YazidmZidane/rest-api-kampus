package com.yazid.RestApiKampus.services;

import com.yazid.RestApiKampus.models.Role;

public interface RoleService {
    Role findByName(String name);
}
