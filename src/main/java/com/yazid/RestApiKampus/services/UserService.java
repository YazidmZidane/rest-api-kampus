package com.yazid.RestApiKampus.services;

import java.util.List;

import com.yazid.RestApiKampus.models.UserModel;
import com.yazid.RestApiKampus.models.dto.UserModelDTO;

public interface UserService {
	UserModel save(UserModelDTO user);
    List<UserModel> findAll();
    UserModel findOne(String username);
}
