package com.yazid.RestApiKampus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiKampusApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiKampusApplication.class, args);
	}

}
