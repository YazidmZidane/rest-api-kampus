package com.yazid.RestApiKampus.models.dto;

public class DataNilaiDTO {
	private String nim;
	private String namaMataKuliah;
	private String namaDosen;
	private int nilai;
	private String keterangan;
	
	public String getNim() {
		return nim;
	}
	
	public void setNim(String nim) {
		this.nim = nim;
	}
	
	public String getNamaMataKuliah() {
		return namaMataKuliah;
	}
	
	public void setNamaMataKuliah(String namaMataKuliah) {
		this.namaMataKuliah = namaMataKuliah;
	}
	
	public String getNamaDosen() {
		return namaDosen;
	}
	
	public void setNamaDosen(String namaDosen) {
		this.namaDosen = namaDosen;
	}
	
	public int getNilai() {
		return nilai;
	}
	
	public void setNilai(int nilai) {
		this.nilai = nilai;
	}
	
	public String getKeterangan() {
		return keterangan;
	}
	
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
}
