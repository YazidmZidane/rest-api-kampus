package com.yazid.RestApiKampus.models.dto;

public class AuthToken {
	private String token;

	public AuthToken(String token) {
		super();
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
