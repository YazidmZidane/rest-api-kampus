package com.yazid.RestApiKampus.models.dto;

public class DataNilaiMahasiswa {
	
	private String nim;
	private String nama;
	private String jurusan;
	private int umur;
	private String dosen;
	private String namaMataKuliah;
	private int nilai;
	
	public String getNim() {
		return nim;
	}
	
	public void setNim(String nim) {
		this.nim = nim;
	}
	
	public String getNama() {
		return nama;
	}
	
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	public String getJurusan() {
		return jurusan;
	}
	
	public void setJurusan(String jurusan) {
		this.jurusan = jurusan;
	}
	
	public int getUmur() {
		return umur;
	}
	
	public void setUmur(int umur) {
		this.umur = umur;
	}
	
	public String getDosen() {
		return dosen;
	}
	
	public void setDosen(String dosen) {
		this.dosen = dosen;
	}
	
	public String getNamaMataKuliah() {
		return namaMataKuliah;
	}
	
	public void setNamaMataKuliah(String namaMataKuliah) {
		this.namaMataKuliah = namaMataKuliah;
	}
	
	public int getNilai() {
		return nilai;
	}
	
	public void setNilai(int nilai) {
		this.nilai = nilai;
	}
	
	
}
