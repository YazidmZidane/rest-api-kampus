package com.yazid.RestApiKampus.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "dosen", schema = "public")
@EntityListeners(AuditingEntityListener.class)
public class Dosen {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "dosen_id", unique = true, nullable = false)
	private long dosenId;
	
	private String nama;

	public long getDosenId() {
		return dosenId;
	}

	public void setDosenId(long dosenId) {
		this.dosenId = dosenId;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}
}
