package com.yazid.RestApiKampus.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "mata_kuliah", schema = "public")
@EntityListeners(AuditingEntityListener.class)
public class MataKuliah {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "mata_kuliah_id", unique = true, nullable = false)
	private long mataKuliahId;
	
	private String namaMataKuliah;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "nim")
	private Mahasiswa nim;

	public long getMataKuliahId() {
		return mataKuliahId;
	}

	public void setMataKuliahId(long mataKuliahId) {
		this.mataKuliahId = mataKuliahId;
	}

	public String getNamaMataKuliah() {
		return namaMataKuliah;
	}

	public void setNamaMataKuliah(String namaMataKuliah) {
		this.namaMataKuliah = namaMataKuliah;
	}

	public Mahasiswa getNim() {
		return nim;
	}

	public void setNim(Mahasiswa nim) {
		this.nim = nim;
	}
}
