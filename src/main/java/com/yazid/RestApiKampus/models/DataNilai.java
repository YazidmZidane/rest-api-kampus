package com.yazid.RestApiKampus.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "data_nilai", schema = "public")
@EntityListeners(AuditingEntityListener.class)
public class DataNilai {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "data_nilai_id", unique = true, nullable = false)
	private long dataNilaiId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "nim", nullable = false)
	private Mahasiswa nim;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "mata_kuliah_id", nullable = false)
	private MataKuliah mataKuliahId;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "dosen_id", nullable = false)
	private Dosen dosenId;
	
	private int nilai;
	private String keterangan;

	public Mahasiswa getNim() {
		return nim;
	}

	public void setNim(Mahasiswa nim) {
		this.nim = nim;
	}

	public MataKuliah getMataKuliah() {
		return mataKuliahId;
	}

	public void setMataKuliah(MataKuliah mataKuliah) {
		this.mataKuliahId = mataKuliah;
	}

	public Dosen getDosen() {
		return dosenId;
	}

	public void setDosen(Dosen dosen) {
		this.dosenId = dosen;
	}

	public int getNilai() {
		return nilai;
	}

	public void setNilai(int nilai) {
		this.nilai = nilai;
	}

	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
}
