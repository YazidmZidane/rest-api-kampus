package com.yazid.RestApiKampus.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "mahasiswa", schema = "public")
@EntityListeners(AuditingEntityListener.class)
public class Mahasiswa {
	
	@Id
	@Column(name = "nim", unique = true, nullable = false)
	private String nim;
	
	private String nama;
	private String alamat;
	private Date tanggalLahir;
	private String jurusan;
	
	public String getNim() {
		return nim;
	}
	
	public void setNim(String nim) {
		this.nim = nim;
	}
	
	public String getNama() {
		return nama;
	}
	
	public void setNama(String nama) {
		this.nama = nama;
	}
	
	public String getAlamat() {
		return alamat;
	}
	
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	
	public Date getTanggalLahir() {
		return tanggalLahir;
	}
	
	public void setTanggalLahir(Date tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}
	
	public String getJurusan() {
		return jurusan;
	}
	
	public void setJurusan(String jurusan) {
		this.jurusan = jurusan;
	}
	
	
}
