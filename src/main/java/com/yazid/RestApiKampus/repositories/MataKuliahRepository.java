package com.yazid.RestApiKampus.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.yazid.RestApiKampus.models.MataKuliah;

@Repository
public interface MataKuliahRepository extends JpaRepository<MataKuliah, Long> {
	
	@Query(value = "SELECT * FROM mata_kuliah WHERE nama_mata_kuliah = :nama AND nim = :nim", nativeQuery = true)
	public MataKuliah getByMatkulNama(@Param(value = "nama") String nama, @Param(value = "nim") String nim);
}
