package com.yazid.RestApiKampus.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.yazid.RestApiKampus.models.Mahasiswa;

@Repository
public interface MahasiswaRepository extends JpaRepository<Mahasiswa, String> {

}
