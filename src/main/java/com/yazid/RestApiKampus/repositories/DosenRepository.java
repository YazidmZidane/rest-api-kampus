package com.yazid.RestApiKampus.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.yazid.RestApiKampus.models.Dosen;

@Repository
public interface DosenRepository extends JpaRepository<Dosen, Long> {
	
	@Query(value = "SELECT * FROM dosen WHERE nama = :nama", nativeQuery = true)
	public Dosen getByDosenNama(@Param(value = "nama") String nama);
}
