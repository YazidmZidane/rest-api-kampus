package com.yazid.RestApiKampus.repositories;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.yazid.RestApiKampus.models.DataNilai;

@Repository
public interface DataNilaiRepository extends JpaRepository<DataNilai, Long> {

	@Query(value = "SELECT dn.* " + 
			"FROM data_nilai dn " + 
			"LEFT JOIN mahasiswa m ON dn.nim = m.nim " + 
			"LEFT JOIN dosen d on dn.dosen_id = d.dosen_id " + 
			"LEFT JOIN mata_kuliah mk ON dn.mata_kuliah_id = mk.mata_kuliah_id " + 
			"ORDER BY dn.nilai", nativeQuery = true)
	List<DataNilai> getDataNilaiMahasiswa();
	
	@Query(value = "SELECT AVG(nilai) AS nilai_rata_rata, m.nama FROM data_nilai dn LEFT JOIN mahasiswa m ON dn.nim = m.nim GROUP BY dn.nim", nativeQuery = true)
	List<Map<String,Object>> getRataRataByMahasiswa();
	
	@Query(value = "SELECT AVG(nilai) AS nilai_rata_rata, m.jurusan FROM data_nilai dn LEFT JOIN mahasiswa m ON dn.nim = m.nim GROUP BY m.jurusan", nativeQuery = true)
	List<Map<String,Object>> getRataRataByJurusan();
}
